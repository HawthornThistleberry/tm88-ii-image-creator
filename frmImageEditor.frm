VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmImageEditor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TM88-II Image Editor"
   ClientHeight    =   7830
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10350
   Icon            =   "frmImageEditor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7830
   ScaleWidth      =   10350
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAbout 
      Caption         =   "About"
      Height          =   375
      Left            =   120
      TabIndex        =   15
      Top             =   7320
      Width           =   2295
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   120
      TabIndex        =   14
      Top             =   2880
      Width           =   2295
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   1920
      Top             =   5400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSaveAs 
      Caption         =   "Save As"
      Height          =   375
      Left            =   120
      TabIndex        =   13
      Top             =   3840
      Width           =   2295
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Open"
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   3360
      Width           =   2295
   End
   Begin VB.PictureBox picImage 
      BackColor       =   &H80000005&
      Height          =   7680
      Left            =   2640
      MousePointer    =   2  'Cross
      ScaleHeight     =   508
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   508
      TabIndex        =   1
      Top             =   120
      Width           =   7680
   End
   Begin VB.Frame fraImageInfo 
      Caption         =   "Image Information"
      Height          =   2775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2535
      Begin VB.HScrollBar hscColumns 
         Height          =   255
         LargeChange     =   16
         Left            =   120
         Max             =   512
         Min             =   1
         TabIndex        =   8
         Top             =   1920
         Value           =   512
         Width           =   2295
      End
      Begin VB.HScrollBar hscRows 
         Height          =   255
         Left            =   120
         Max             =   64
         Min             =   1
         TabIndex        =   5
         Top             =   1200
         Value           =   64
         Width           =   2295
      End
      Begin VB.ComboBox cmbMode 
         Height          =   315
         ItemData        =   "frmImageEditor.frx":0442
         Left            =   120
         List            =   "frmImageEditor.frx":0454
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label lblFileSizeCount 
         Alignment       =   1  'Right Justify
         Caption         =   "32773 bytes"
         Height          =   255
         Left            =   960
         TabIndex        =   11
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label lblFileSize 
         Caption         =   "File Size:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2400
         Width           =   735
      End
      Begin VB.Label lblColumnCount 
         Alignment       =   1  'Right Justify
         Caption         =   "512 dots"
         Height          =   255
         Left            =   960
         TabIndex        =   9
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label lblColumns 
         Caption         =   "Columns:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label lblRowCount 
         Alignment       =   1  'Right Justify
         Caption         =   "512 dots (64 rows)"
         Height          =   255
         Left            =   960
         TabIndex        =   6
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lblRows 
         Caption         =   "Rows:"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblMode 
         Caption         =   "Mode:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmImageEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const conAppName = "TM88-II Image Editor"

'-- to do
' Save
' Open: load
' Copy, Paste
' handle "dirty" flag
' touch-up: draw, erase, and text modes
' About
' Help

Private Sub Form_Load()
  On Error Resume Next
  Left = GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  cmbMode.ListIndex = 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
End Sub

Private Sub cmbMode_Click()
  If hscColumns > 256 And cmbMode.ItemData(cmbMode.ListIndex) Mod 2 = 0 Then
      ' switch to the corresponding double-density mode (1 or 33)
      hscColumns = 256
    End If
End Sub

Private Sub hscRows_Scroll()
  ' update the display label
  lblRowCount = CStr(hscRows * 8) & " dots (" & CStr(hscRows) & " row"
  If hscRows > 1 Then lblRowCount = lblRowCount & "s"
  lblRowCount = lblRowCount & ")"
  Call lblFileSizeCount_Update
End Sub

Private Sub hscColumns_Scroll()
  ' update the display label
  lblColumnCount = CStr(hscColumns) & " dot"
  If hscColumns > 1 Then lblColumnCount = lblColumnCount & "s"
  Call lblFileSizeCount_Update
End Sub

Private Sub lblFileSizeCount_Update()
  Dim bytecount As Long
  If cmbMode.ItemData(cmbMode.ListIndex) <= 1 Then
      bytecount = CLng(hscRows) * (CLng(hscColumns) + 7)
    Else
      bytecount = CLng(Int(hscRows / 3 + 0.99)) * (CLng(hscColumns) * 3 + 7)
    End If
  lblFileSizeCount = CStr(bytecount) & " bytes"
End Sub

Private Sub hscRows_Change()
  hscRows_Scroll
  picImage.Height = picImage.ScaleY(8 * hscRows - 1, vbPixels, vbTwips)
End Sub

Private Sub hscColumns_Change()
  hscColumns_Scroll
  picImage.Width = picImage.ScaleX(hscColumns - 1, vbPixels, vbTwips)
End Sub

Private Sub cmdClear_Click()
  If MsgBox("Erase image?", vbOKCancel + vbQuestion, conAppName) <> vbOK Then Exit Sub
  picImage.Picture = LoadPicture()
End Sub

Private Sub cmdOpen_Click()
  cdlGeneric.DialogTitle = "Open image"
  cdlGeneric.FileName = ""
  cdlGeneric.DefaultExt = "*.pti"
  cdlGeneric.Filter = "Epson TM88-II images (*.pti)|*.pti|Bitmap files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif|JPEG files (*.jpg)|*.jpg|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + _
      cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.FileName = "" Then Exit Sub
  picImage.Height = picImage.ScaleY(512, vbPixels, vbTwips)
  picImage.Width = picImage.ScaleX(512, vbPixels, vbTwips)
  If LCase(Right(cdlGeneric.FileName, 4)) <> ".pti" Then
      picImage.Picture = LoadPicture(cdlGeneric.FileName, , vbLPMonochrome)
      picImage_Cleanup
    End If
  '-- handle PTI loads
  Exit Sub
cancelled:
  Exit Sub
End Sub

Private Sub picImage_Cleanup()
  Dim r As Integer, g As Integer, b As Integer
  Dim X As Integer, Y As Integer
  Dim clr As Long
  Dim rows As Integer, cols As Integer
  
  ' convert to black and white
  MousePointer = vbHourglass
  picImage.MousePointer = vbHourglass
  frmImageEditor.Caption = conAppName & " - Cleaning up image..."
  DoEvents
  picImage.AutoRedraw = True
  picImage.ScaleMode = vbPixels
  rows = 0
  cols = 0
  For Y = 0 To picImage.ScaleHeight - 1
    For X = 0 To picImage.ScaleWidth - 1
      ' get the source pixel's color components
      clr = picImage.Point(X, Y)
      r = clr Mod 256
      g = (clr \ 256) Mod 256
      b = clr \ 256 \ 256
      ' figure out if it should be a black dot
      clr = RGB(255, 255, 255)
      If r + g + b < 384 Then
          clr = RGB(0, 0, 0)
          ' figure out the drawn image size
          If Y > rows Then rows = Y
          If X > cols Then cols = X
        End If
      ' write the new pixel
      picImage.PSet (X, Y), clr
      Next X
    DoEvents
    Next Y
  picImage.Picture = picImage.Image
  frmImageEditor.Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair

  ' set the mode, rows, and columns
  hscColumns = cols + 6
  hscRows = Int((rows + 6) / 8 + 0.9999999)
  ' if we've chosen a single-density mode (0 or 32) but have >256 columns,
  If hscColumns > 256 And cmbMode.ItemData(cmbMode.ListIndex) Mod 2 = 0 Then
      ' switch to the corresponding double-density mode (1 or 33)
      cmbMode.ListIndex = cmbMode.ListIndex + 1
    End If
  hscRows_Change
  hscColumns_Change
End Sub

Private Sub cmdSaveAs_Click()
  Dim rowY As Integer, bitY As Integer
  Dim errtext As String
  Dim outerrow As Integer, outerrows As Integer
  Dim innerrow As Integer, innerrows As Integer, padrows As Integer
  Dim column As Integer, bitnum As Integer, byteval As Byte
  
  cdlGeneric.DialogTitle = "Save image"
  cdlGeneric.FileName = ""
  cdlGeneric.DefaultExt = "*.pti"
  cdlGeneric.Filter = "Epson TM88-II images (*.pti)|*.pti|Bitmap files (*.bmp)|*.bmp|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.FileName = "" Then Exit Sub
  If LCase(Right(cdlGeneric.FileName, 4)) <> ".pti" Then
      SavePicture picImage, cdlGeneric.FileName
      Exit Sub
    End If
  ' pti save
  On Error GoTo fileerror
  ' open the file for binary output
  errtext = "opening file"
  Open cdlGeneric.FileName For Binary Access Write Lock Write As #1
  ' set up the screen for the save
  errtext = "setting up screen"
  MousePointer = vbHourglass
  picImage.MousePointer = vbHourglass
  frmImageEditor.Caption = conAppName & " - Saving..."
  DoEvents
  picImage.ScaleMode = vbPixels
  ' figure out how many rows we have
  If cmbMode.ItemData(cmbMode.ListIndex) <= 1 Then
      outerrows = hscRows
      innerrows = 1
      padrows = 0
    Else
      outerrows = Int(hscRows / 3 + 0.99)
      innerrows = 3
      padrows = outerrows * 3 - hscRows
    End If
  For outerrow = 1 To outerrows
    ' write out first few bytes: image code, then mode
    errtext = "writing row header"
    '-- Put not Print
    Print #1, Chr(27); Chr(42); Chr(cmbMode.ItemData(cmbMode.ListIndex));
    ' write out the number of bytes to follow in this outerrow
    column = hscColumns * innerrows ' # bytes in this outerrow
    byteval = column Mod 256 ' low byte of # bytes
    Print #1, Chr(byteval);
    byteval = column - 256 * byteval ' high byte of # bytes
    Print #1, Chr(byteval);
    errtext = "writing row body"
    For innerrow = 1 To innerrows
      ' handle if we're past the real end because of those pad rows in high modes
      If outerrow = outerrows And innerrow > 3 - padrows Then
          ' this is a padding row to make an integral multiple of 3 rows, so it's all blanks
          For column = 1 To hscColumns
            Print #1, Chr(0);
            Next column
        Else ' a real row
          ' figure out the Y of the top of the row
          rowY = (outerrow - 1) * 24 + (innerrow - 1) * 8
          For column = 1 To hscColumns
            byteval = 0
            For bitnum = 0 To 7
              bitY = rowY + (7 - bitnum) ' MSB is on top, annoyingly
              If picImage.Point(column - 1, bitY) = RGB(0, 0, 0) Then byteval = byteval + 2 ^ bitnum
              Next bitnum
            Print #1, Chr(byteval);
            Next column
        End If
      Next innerrow
    errtext = "writing row trailer"
    Print #1, Chr(13); Chr(10);
    DoEvents
    Next outerrow
  errtext = "closing file"
  Close #1
  ' clean up the screen
  errtext = "cleaning up screen"
  frmImageEditor.Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair
  Exit Sub
cancelled:
  Exit Sub
fileerror:
  MsgBox "Error #" & CStr(Err) & " " & errtext & ": " & Error(Err), vbExclamation, conAppName
  On Error GoTo 0
  Close #1
  frmImageEditor.Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair
  Exit Sub
End Sub

