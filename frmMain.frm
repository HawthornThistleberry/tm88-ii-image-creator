VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TM88-II Image Creator"
   ClientHeight    =   7905
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10425
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   ScaleHeight     =   7905
   ScaleWidth      =   10425
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdPaste 
      Caption         =   "Paste"
      Height          =   375
      Left            =   120
      TabIndex        =   7
      Top             =   6600
      Width           =   2415
   End
   Begin VB.CommandButton cmdCopy 
      Caption         =   "Copy"
      Height          =   375
      Left            =   120
      TabIndex        =   6
      Top             =   6120
      Width           =   2415
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "About"
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   7440
      Width           =   2415
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear"
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   4320
      Width           =   2415
   End
   Begin MSComDlg.CommonDialog cdlGeneric 
      Left            =   2040
      Top             =   2880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSaveAs 
      Caption         =   "Save As"
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   5280
      Width           =   2415
   End
   Begin VB.CommandButton cmdOpen 
      Caption         =   "Open"
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   4800
      Width           =   2415
   End
   Begin VB.PictureBox picImage 
      BackColor       =   &H80000005&
      Height          =   7755
      Left            =   2640
      MousePointer    =   2  'Cross
      ScaleHeight     =   513
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   513
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   120
      Width           =   7755
   End
   Begin VB.Frame fraImageInfo 
      Caption         =   "Image Information"
      Height          =   2775
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   2535
      Begin VB.HScrollBar hscColumns 
         Height          =   255
         LargeChange     =   16
         Left            =   120
         Max             =   512
         Min             =   1
         TabIndex        =   2
         Top             =   1920
         Value           =   512
         Width           =   2295
      End
      Begin VB.HScrollBar hscRows 
         Height          =   255
         Left            =   120
         Max             =   64
         Min             =   1
         TabIndex        =   1
         Top             =   1200
         Value           =   64
         Width           =   2295
      End
      Begin VB.ComboBox cmbMode 
         Height          =   315
         ItemData        =   "frmMain.frx":0442
         Left            =   120
         List            =   "frmMain.frx":0454
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label lblFileSizeCount 
         Alignment       =   1  'Right Justify
         Caption         =   "32773 bytes"
         Height          =   255
         Left            =   960
         TabIndex        =   17
         Top             =   2400
         Width           =   1455
      End
      Begin VB.Label lblFileSize 
         Caption         =   "File Size:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2400
         Width           =   735
      End
      Begin VB.Label lblColumnCount 
         Alignment       =   1  'Right Justify
         Caption         =   "512 dots"
         Height          =   255
         Left            =   960
         TabIndex        =   15
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label lblColumns 
         Caption         =   "Columns:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1680
         Width           =   735
      End
      Begin VB.Label lblRowCount 
         Alignment       =   1  'Right Justify
         Caption         =   "512 dots (64 rows)"
         Height          =   255
         Left            =   960
         TabIndex        =   13
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label lblRows 
         Caption         =   "Rows:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   735
      End
      Begin VB.Label lblMode 
         Caption         =   "Mode:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Line Line2 
      X1              =   120
      X2              =   2520
      Y1              =   5880
      Y2              =   5880
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   2520
      Y1              =   7200
      Y2              =   7200
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' TM88II Image Creator
' Copyright (c) 2001 by Vermont Department of Liquor Control

' Permission is granted to use this program for any purpose freely.
' You may also modify it for internal use, but you may not distribute a modified version
' without getting permission from us.  (We just want to get credit for our work and
' not get in trouble for it.)

' Note: though this program is designed to generate print files for Epson TM88-II
' printers, no affiliation with Epson is expressed or implied.

' Visit us at http://www.state.vt.us/dlc/ or write at it@dlc.state.vt.us.

' Program written during 2002-02 by Frank J. Perricone (frank@dlc.state.vt.us).

' PURPOSE:
' Print routines written by VT DLC for cash registers include the ability to send a binary
' inclusion file, byte for byte, to the printer.  (There's also facilities to have
' different binary files for different printers, which presumably do the same thing.)
' One of the main purposes of this facility is to be able to print images, which are otherwise
' nearly impossible to generate from marked-up text.  Therefore, we need to be able to
' generate a binary file which contains the exact bytes necessary to do print an image.
' This can be done by tedious fiddling in hex editors, but this way is much better.
' Rather than try to replicate an entire picture editor program, I decided it would be better
' to make a program which simply let me convert an existing image created by some other
' program into the required format.  The necessities of setting print modes, dealing with the
' conversion to raw black-and-white, and being sure you get what you expect, meant I needed
' to do this as a "crippled image editor" -- it looks like an editor but it's really only a
' preview you're looking at.  However, by passing back and forth with copy and paste, you can
' get a pretty dynamic editing environment.  Why you'd want to is beyond me -- get the picture
' right in your paint program, and only then convert it, that's what I think works best.

' Given how small the market for this program likely is (it might consist solely of me!)
' I decided not to bother with making a help file, putting a more obvious UI that makes
' clear how the window is a preview only, or make something snazzier looking than those big
' ugly command buttons.  It works and it's pretty enough to work well, and that's good enough
' for now.

Option Explicit

Const conAppName = "TM88-II Image Creator"

Dim LastFilename As String
Dim Dirty As Boolean

Private Sub Form_Load()
  On Error Resume Next
  ' position the window where it was last, or if it was never positioned before, in the middle
  Left = GetSetting(conAppName, "Startup", "Left", (Screen.Width - Width) / 2)
  Top = GetSetting(conAppName, "Startup", "Top", (Screen.Height - Height) / 2)
  ' restore the last filename used previously
  LastFilename = GetSetting(conAppName, "Startup", "LastFilename", "")
  ' start in mode 1
  cmbMode.ListIndex = 1
  Dirty = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
  Dim result As Integer
  If Dirty Then
      result = MsgBox("Image is not saved.  Save first?", vbYesNoCancel + vbExclamation, conAppName)
      If result = vbCancel Then
          Cancel = True
          Exit Sub
        End If
      If result = vbYes Then
          Call cmdSaveAs_Click
          If Dirty Then ' only possible if save failed or was cancelled
              Cancel = True
              Exit Sub
            End If
        End If
    End If
  ' save settings in registry
  Call SaveSetting(conAppName, "Startup", "Left", Left)
  Call SaveSetting(conAppName, "Startup", "Top", Top)
  Call SaveSetting(conAppName, "Startup", "LastFilename", LastFilename)
End Sub

Private Sub cmbMode_Click()
  ' if we've changed to a mode that only supports 256 dots across but we have more than that,
  If hscColumns > 256 And cmbMode.ItemData(cmbMode.ListIndex) Mod 2 = 0 Then
      ' cut off the image
      hscColumns = 256
    End If
  ' update the byte count shown
  Call lblFileSizeCount_Update
  Dirty = True
End Sub

Private Sub hscRows_Scroll()
  ' update the display label
  lblRowCount = CStr(hscRows * 8) & " dots (" & CStr(hscRows) & " row"
  If hscRows > 1 Then lblRowCount = lblRowCount & "s"
  lblRowCount = lblRowCount & ")"
  ' update the byte count shown
  Call lblFileSizeCount_Update
End Sub

Private Sub hscColumns_Scroll()
  ' update the display label
  lblColumnCount = CStr(hscColumns) & " dot"
  If hscColumns > 1 Then lblColumnCount = lblColumnCount & "s"
  ' update the byte count shown
  Call lblFileSizeCount_Update
End Sub

Private Sub lblFileSizeCount_Update()
  ' update the byte count shown
  Dim bytecount As Long
  If cmbMode.ItemData(cmbMode.ListIndex) <= 1 Then
      bytecount = CLng(hscRows) * (CLng(hscColumns) + 6) + 5
    Else
      bytecount = CLng(Int(hscRows / 3 + 0.99)) * (CLng(hscColumns) * 3 + 6) + 5
    End If
  lblFileSizeCount = CStr(bytecount) & " bytes"
End Sub

Private Sub hscRows_Change()
  hscRows_Scroll
  ' cut off the image to the specified size, for WYSIWYG purposes
  picImage.Height = picImage.ScaleY(8 * hscRows - 1 + 5, vbPixels, vbTwips)
  Dirty = True
End Sub

Private Sub hscColumns_Change()
  hscColumns_Scroll
  ' cut off the image to the specified size, for WYSIWYG purposes
  picImage.Width = picImage.ScaleX(hscColumns - 1 + 5, vbPixels, vbTwips)
  Dirty = True
End Sub

Private Sub cmdClear_Click()
  If MsgBox("Erase image?", vbOKCancel + vbQuestion, conAppName) <> vbOK Then Exit Sub
  picImage.Picture = LoadPicture()
  Dirty = False
End Sub

Private Sub cmdOpen_Click()
  ' build an open dialog
  cdlGeneric.DialogTitle = "Open image"
  cdlGeneric.FileName = LastFilename
  cdlGeneric.DefaultExt = "*.gif"
  cdlGeneric.Filter = "GIF files (*.gif)|*.gif|JPEG files (*.jpg)|*.jpg|Bitmap files (*.bmp)|*.bmp|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNFileMustExist + cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames
  cdlGeneric.CancelError = True
  ' show the open dialog
  On Error GoTo cancelled
  cdlGeneric.ShowOpen
  If cdlGeneric.FileName = "" Then Exit Sub
  ' save the new last filename
  LastFilename = cdlGeneric.FileName
  ' reset the window to full size (5 larger than real size for picture-box overhead)
  picImage.Height = picImage.ScaleY(517, vbPixels, vbTwips)
  picImage.Width = picImage.ScaleX(517, vbPixels, vbTwips)
  ' load the image
  picImage.Picture = LoadPicture(cdlGeneric.FileName, , vbLPMonochrome)
  ' change to black and white and record the actual size
  picImage_Cleanup
  Exit Sub
cancelled:
  Exit Sub
End Sub

Private Sub picImage_Cleanup()
  ' convert to black and white; while doing so, record the highest black-dot X and Y coordinates,
  ' as that gives us the effective size of the image; note that the LoadPicture automatically
  ' cuts off images that are too big for the full-sized picture box, so no need to worry about that
  Dim r As Integer, g As Integer, b As Integer
  Dim X As Integer, Y As Integer
  Dim clr As Long
  Dim rows As Integer, cols As Integer
  
  ' make yourself look busy!
  MousePointer = vbHourglass
  picImage.MousePointer = vbHourglass
  Caption = conAppName & " - Cleaning up image..."
  DoEvents
  ' prepare the picture box and the variables that'll count the picture size
  picImage.AutoRedraw = True
  picImage.ScaleMode = vbPixels
  rows = 0
  cols = 0
  ' go through the image
  For Y = 0 To picImage.ScaleHeight - 1
    For X = 0 To picImage.ScaleWidth - 1
      ' get the source pixel's color components
      clr = picImage.Point(X, Y)
      r = clr Mod 256
      g = (clr \ 256) Mod 256
      b = clr \ 256 \ 256
      ' figure out if it should be a black dot
      clr = RGB(255, 255, 255)
      If r + g + b < 384 Then
          clr = RGB(0, 0, 0)
          ' figure out the drawn image size
          If Y > rows Then rows = Y
          If X > cols Then cols = X
        End If
      ' write the new pixel
      picImage.PSet (X, Y), clr
      Next X
    DoEvents
    Next Y
  ' save the changed picture
  picImage.Picture = picImage.Image
  ' restore all the "busy" stuff
  Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair

  ' set the mode, rows, and columns
  hscColumns = cols + 1
  hscRows = Int((rows + 1) / 8 + 0.9999999)
  ' if we've chosen a single-density mode (0 or 32) but have >256 columns,
  If hscColumns > 256 And cmbMode.ItemData(cmbMode.ListIndex) Mod 2 = 0 Then
      ' switch to the corresponding double-density mode (1 or 33)
      cmbMode.ListIndex = cmbMode.ListIndex + 1
    ' if we've chosen a double-density mode but have <=256 columns
    ElseIf hscColumns <= 256 And cmbMode.ItemData(cmbMode.ListIndex) Mod 2 <> 0 Then
      ' switch to the corresponding double-density mode (0 or 32)
      cmbMode.ListIndex = cmbMode.ListIndex - 1
      ' this one isn't necessary but is usually what the user wants
    End If
  hscRows_Change
  hscColumns_Change
  Dirty = True
End Sub

Private Sub cmdCopy_Click()
  picImage.Picture = picImage.Image
  Clipboard.SetData picImage.Picture, vbCFBitmap
  Beep ' so you can tell it's happened
End Sub

Private Sub cmdPaste_Click()
  ' verify there's something pasteable
  If Not Clipboard.GetFormat(vbCFBitmap) Then
      MsgBox "No picture information on clipboard; cannot paste.", vbOKOnly + vbInformation, conAppName
      Exit Sub
    End If
  ' reset the window to full size (5 larger than real size for picture-box overhead)
  picImage.Height = picImage.ScaleY(517, vbPixels, vbTwips)
  picImage.Width = picImage.ScaleX(517, vbPixels, vbTwips)
  ' paste the image
  picImage.Picture = Clipboard.GetData(vbCFBitmap)
  ' clean up after the paste
  picImage_Cleanup
  Beep ' so you can tell it's happened... though it should be obvious, ne c'est pas?
End Sub

Private Sub cmdSaveAs_Click()
  Dim rowY As Integer, bitY As Integer
  Dim errtext As String
  Dim outerrow As Integer, outerrows As Integer
  Dim innerrow As Integer, innerrows As Integer, padrows As Integer
  Dim column As Integer, bitnum As Integer, byteval As Byte
  
  ' build a save dialog box
  cdlGeneric.DialogTitle = "Save image"
  cdlGeneric.FileName = StripExtension(LastFilename) & ".pti"
  cdlGeneric.DefaultExt = "*.pti"
  cdlGeneric.Filter = "Epson TM88-II images (*.pti)|*.pti|Bitmap files (*.bmp)|*.bmp|All Files (*.*)|*.*"
  cdlGeneric.FilterIndex = 1
  cdlGeneric.Flags = cdlOFNHideReadOnly + cdlOFNPathMustExist + cdlOFNExplorer + cdlOFNLongNames + cdlOFNOverwritePrompt
  cdlGeneric.CancelError = True
  ' show the save dialog box
  On Error GoTo cancelled
  cdlGeneric.ShowSave
  If cdlGeneric.FileName = "" Then Exit Sub
  ' handle saving as a bitmap (probably will never be used, but it's easy to provide)
  If LCase(Right(cdlGeneric.FileName, 4)) <> ".pti" Then
      SavePicture picImage, cdlGeneric.FileName
      Exit Sub
    End If
  ' pti save -- the real point of the exercise
  On Error GoTo fileerror
  ' open the file for binary output
  errtext = "opening file"
  Open cdlGeneric.FileName For Output Access Write Lock Write As #1
  ' set up the screen for the save
  errtext = "setting up screen"
  MousePointer = vbHourglass
  picImage.MousePointer = vbHourglass
  Caption = conAppName & " - Saving..."
  DoEvents
  picImage.ScaleMode = vbPixels
  ' figure out how many rows we have, accounting for outer and inner rows in modes 32+
  If cmbMode.ItemData(cmbMode.ListIndex) <= 1 Then
      outerrows = hscRows
      innerrows = 1
      padrows = 0
    Else
      outerrows = Int(hscRows / 3 + 0.99)
      innerrows = 3
      padrows = outerrows * 3 - hscRows
    End If
  ' write the vertical compression characters
  errtext = "writing image header"
  Print #1, Chr(27); Chr(51); Chr(22);
  ' for each of the outer rows (which will be a separate image code, bytes, and line feed
  For outerrow = 1 To outerrows
    ' write out first few bytes: image code, then mode
    errtext = "writing row header"
    Print #1, Chr(27); Chr(42); Chr(cmbMode.ItemData(cmbMode.ListIndex));
    ' write out the number of bytes to follow in this outerrow
    errtext = "writing byte counts"
    rowY = hscColumns Mod 256 ' low byte of # bytes
    Print #1, Chr(rowY);
    rowY = (hscColumns - rowY) / 256 ' high byte of # bytes
    Print #1, Chr(rowY);
    errtext = "writing row body"
    ' for each of the columns
    For column = 1 To hscColumns
      ' in higher modes, we have to do three bytes down; these are the inner rows
      For innerrow = 1 To innerrows
        ' figure out the Y of the top of the row
        rowY = (outerrow - 1) * 8 * innerrows + (innerrow - 1) * 8
        ' handle if we're past the real end because of those pad rows in high modes
        If outerrow = outerrows And innerrow > 3 - padrows Then
            ' this is a padding row to make an integral multiple of 3 rows, so it's all blanks
            Print #1, Chr(0);
          Else ' a real row; build a byte
            byteval = 0
            For bitnum = 0 To 7
              bitY = rowY + (7 - bitnum) ' MSB is on top, annoyingly
              If picImage.Point(column - 1, bitY) = RGB(0, 0, 0) Then byteval = byteval + 2 ^ bitnum
              Next bitnum
            Print #1, Chr(byteval);
          End If
        Next innerrow
      Next column
    ' end each outer row with a line feed
    errtext = "writing row trailer"
    Print #1, Chr(10);
    DoEvents
    Next outerrow
  errtext = "writing image trailer"
  ' return printing to uncompressed mode
  Print #1, Chr(27); Chr(50);
  errtext = "closing file"
  Close #1
  Dirty = False
  ' clean up the screen
  errtext = "cleaning up screen"
  Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair
  Exit Sub
cancelled:
  Exit Sub
fileerror:
  MsgBox "Error #" & CStr(Err) & " " & errtext & ": " & Error(Err), vbExclamation, conAppName
  On Error GoTo 0
  Close #1
  Caption = conAppName
  MousePointer = vbDefault
  picImage.MousePointer = vbCrosshair
  Exit Sub
End Sub

Private Sub cmdAbout_Click()
  MsgBox "TM88-II Image Creator v1.0" & vbCrLf & _
         "Copyright (c) 2001 by Vermont Department of Liquor Control" & vbCrLf & _
         "http://www.state.vt.us/dlc/    it@dlc.state.vt.us" & vbCrLf & _
         "Permission granted to distribute and use this program freely, but not modify it." & vbCrLf & _
         "(No affiliation with Epson is expressed or implied.  Use at your own risk.  For external use only.)", _
         vbOKOnly + vbInformation, "About " & conAppName
End Sub

Function StripExtension(fname As String) As String
  ' remove the extension from a filename, if there is one
  Dim slash As Integer, dot As Integer
  slash = InStrRev(fname, "\")
  dot = InStrRev(fname, ".")
  If dot <> 0 And dot > slash Then
      StripExtension = Left(fname, dot - 1)
    Else
      StripExtension = fname
    End If
End Function


